// Solutions for homework tasks

// Insert Task 1 solution here
var longestCommonPrefix = function(strs) {
    if (strs.length === 0) return "";

    let prefix = strs[0];
    for (let i = 1; i < strs.length; i++) {
        while (strs[i].indexOf(prefix) !== 0) {
            prefix = prefix.slice(0, prefix.length - 1);
            if (prefix === "") return "";
        }
    }

    return prefix;
};

// Insert Task 2 solution here

var countPrimes = function(n) {
    if (n <= 2) return 0;
    let isPrime = new Array(n).fill(true);
    isPrime[0] = isPrime[1] = false;
    for (let i = 2; i * i < n; i++) {
        if (isPrime[i]) {
            for (let j = i * i; j < n; j += i) {
                isPrime[j] = false;
            }
        }
    }
    let primeCount = 0;
    for (let i = 2; i < n; i++) {
        if (isPrime[i]) primeCount++;
    }

    return primeCount;
};

// Insert Task 3 solution here
var romanToInt = function(s) {
    const romanNumerals = {
        'I': 1,
        'IV': 4,
        'V': 5,
        'IX': 9,
        'X': 10,
        'XL': 40,
        'L': 50,
        'XC': 90,
        'C': 100,
        'CD': 400,
        'D': 500,
        'CM': 900,
        'M': 1000,
    };

    let result = 0;
    let i = 0;

    while (i < s.length) {
        if (i < s.length - 1 && romanNumerals[s.substring(i, i + 2)]) {
            result += romanNumerals[s.substring(i, i + 2)];
            i += 2;
        } else {
            result += romanNumerals[s[i]];
            i++;
        }
    }

    return result;
};


// Insert Task 4 solution here

var merge = function(nums1, m, nums2, n) {
    let i = 0;
    let j = 0;
    let k = 0;
    let nums1Copy = nums1.slice(0, m);

    while (i < m && j < n) {
        if (nums1Copy[i] <= nums2[j]) {
            nums1[k] = nums1Copy[i];
            i++;
        } else {
            nums1[k] = nums2[j];
            j++;
        }
        k++;
    }

    while (i < m) {
        nums1[k] = nums1Copy[i];
        i++;
        k++;
    }

    while (j < n) {
        nums1[k] = nums2[j];
        j++;
        k++;
    }
    return nums1;
};

// Insert Task 5 solution here

var Solution = function(nums) {
    this.originalArr = nums.slice();
    this.currentArr = nums;
};

Solution.prototype.reset = function() {
    this.currentArr = this.originalArr.slice();
    return this.currentArr;
};

Solution.prototype.shuffle = function() {
    const shuffled = this.currentArr.slice();
    for (let i = shuffled.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [shuffled[i], shuffled[j]] = [shuffled[j], shuffled[i]];
    }
    return shuffled;
};

